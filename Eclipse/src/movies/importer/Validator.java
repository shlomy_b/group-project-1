package movies.importer;
import java.util.ArrayList;
/**
 * Validator extends the Processor class.
 * It validates the data in the file of
 * movies to insure that there are no
 * Abnormalities, such as null movies.
 * 
 * 
 * @author Aharon Moryoussef
 * @since	2020-11-08
 */

public class Validator extends Processor {

	/**
	 * The Validator constructor inherits from the Processor class
	 * @param src
	 * @param destination
	 */
	public Validator(String src , String destination){
		super(src,destination,false);
	}
	
	/**
	 * @param input an ArrayList<String>
	 * @return validate an ArrayList<String> that removes any invalid 
	 */
	public ArrayList<String> process (ArrayList<String> input) {
		String temp[];
		temp = new String[100];
		ArrayList<String> validate = new ArrayList<String>();
		for(int i = 0; i < input.size(); i++) {
			temp = input.get(i).split("\\t",-1);
		
			if(temp[1].equals(null) || temp[1].equals("")) {
				System.out.println("title");
		    }else if(temp[0].equals(null) || temp[0].equals(" ") || temp[0].equals("")) {
		    	System.out.println("release year");
		    }else if (temp[2].equals(null) || temp[2].equals(" ") || temp[2].equals("")) {
		    	System.out.println("run time");
		    }else {
		    	try {
		    		 Integer.parseInt(temp[1]);
		    		 Integer.parseInt(temp[2]);
		    		 //Will only get to this part of the code if there's no exceptions
		    		 Movie movie = new Movie(temp[1],temp[0],temp[2],"imdb");
		    		 validate.add(movie.toString());
		    	}catch(Exception e) {
		    	
		    	}
		    }
		}
		return validate;
	}
}