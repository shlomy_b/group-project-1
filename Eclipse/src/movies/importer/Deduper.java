package movies.importer;
import java.util.ArrayList;
/**
 * Deduper extends the Processor class.
 * It gets rid of duplicates and merges 
 * movies together.
 * 
 * @author Aharon Moryoussef/Shlomo Bensimhon
 * @since	2020-11-08
 *
 */

public class Deduper extends Processor {

	public Deduper(String src, String destination) {
		super(src,destination,false);
	}
	
	public ArrayList<String> process (ArrayList<String> input){
		ArrayList<String> tempList = new ArrayList<String>();
		
		String[] originalMovie;
		String[] duplicateMovie;
		
		for(int i = 0; i < input.size(); i++) {
			if(tempList.contains(input.get(i))) {
				
				System.out.println("Found a duplicate");
				int index = tempList.indexOf(input.get(i)); // get index of original movie
				
				System.out.println("index of original movie: " +  tempList.indexOf(input.get(index)));
				System.out.println("index of depulicate movie: " +  tempList.indexOf(input.get(i)));
				
				originalMovie = tempList.get(index).toString().split("\\t", -1);
				duplicateMovie = input.get(i).toString().split("\\t", -1);
				
				if(originalMovie[3].toString().equals(duplicateMovie[3].toString())) {
					Movie tempMovie = new Movie(originalMovie[1], originalMovie[0], originalMovie[2], originalMovie[3]);
					tempList.set(index, tempMovie.toString());
					System.out.println("same source");
				}else {
					Movie tempMovie = new Movie(originalMovie[1], originalMovie[0], originalMovie[2], originalMovie[3].concat(duplicateMovie[3]));
					tempList.set(index, tempMovie.toString());
					System.out.println("different source");
				}
			}else {
				tempList.add(input.get(i));
				System.out.println("added new movie");
			}
		}
		return tempList;
	}
}