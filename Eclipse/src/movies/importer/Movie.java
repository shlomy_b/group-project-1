package movies.importer;
/**
 * 
 * @author Aharon Moryoussef/Shlomo Bensimhon
 * @since 	2020-11-08
 *
 */
public class Movie {

	private String releaseYear;
	private String movieName;
	private String runtime;
	private String source;
	
	public Movie(String releaseYear, String movieName, String runtime, String source) {
		this.releaseYear = releaseYear;
		this.movieName = movieName;
		this.runtime = runtime;
		this.source = source;
	}
	
	public void setRelease(String releaseDate) {
		this.releaseYear = releaseDate;
	}
	
	public void setMovieName(String movieName) {
		this.movieName = movieName;
	}
	
	public void setRuntime(String runtime) {
		this.runtime = runtime;
	}
	
	public void setSource(String source) {
		this.source = source;
	}
	
	public String getRelease() {
		return this.releaseYear;
	}
	
	public String getMovieName() {
		return this.movieName;
	}
	
	public String getRuntime() {
		return this.runtime;
	}
	
	public String getSource() {
		return this.source;
	}
	
	public String toString() {
		return (this.movieName + "\t" + this.releaseYear + "\t" + this.runtime + "\t" + this.source); 
	}
	
	public boolean equals (Object obj) {
		Movie cast = (Movie) obj;
		int orginal = Integer.parseInt(this.runtime);
		int nonOg = Integer.parseInt(cast.runtime);
        
        if(this.movieName != cast.movieName || this.releaseYear != cast.releaseYear 
        || (orginal < nonOg + 5 || orginal > nonOg - 5)) {
            return false;
        }else {
            return true;
        }
	}
}









