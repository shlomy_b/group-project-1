
package movies.importer;
import java.io.*;
/**
 * 
 * @author Shlomo Bensimhon
 * @since	2020-11-08
 */

public class TempTester {

	public static void main(String[] args)  throws IOException{
		
		//Normalizer k1 = new Normalizer("C:\\Users\\shlom\\OneDrive\\Desktop\\tempTexts", "C:\\Users\\shlom\\OneDrive\\Desktop\\projectTempTester");
		//k1.execute();
		
		//KaggleImporter k1 = new KaggleImporter("C:\\Users\\shlom\\OneDrive\\Desktop\\tempTexts", "C:\\Users\\shlom\\OneDrive\\Desktop\\projectTempTester");
		//k1.execute();
		
		//ImbdImporter i = new ImbdImporter("C:\\Users\\aron2\\OneDrive\\Documents\\imbdtest" , "C:\\Users\\aron2\\OneDrive\\Documents\\imbd_results");
		//i.execute();
		
		Deduper k1 = new Deduper("C:\\Users\\shlom\\OneDrive\\Desktop\\tempTexts", "C:\\Users\\shlom\\OneDrive\\Desktop\\projectTempTester");
		k1.execute();
		
		//Validator k1 = new Validator("C:\\Users\\shlom\\OneDrive\\Desktop\\tempTexts", "C:\\Users\\shlom\\OneDrive\\Desktop\\projectTempTester");
		//k1.execute();
		
	}

}