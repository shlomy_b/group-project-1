package movies.importer;
import movies.importer.*;
import movies.tests.*;
import java.io.IOException;

/**
 * The ImportPipeline class is the class 
 * where we call all of our methods. The 
 * original files get modified after every 
 * method they pass, giving us out final result.
 * 
 * @author Aharon Moryoussef/Shlomo Bensimhon
 * 
 * @since	2020-11-08
 *
 */

public class ImportPipeline {

    public static void main(String[] args)throws IOException {
        // TODO Auto-generated method stub
        Processor[] all = new Processor[5];

        all[0]= new KaggleImporter("C:\\Users\\shlom\\OneDrive\\Documents\\Git Projects\\group-project-1\\kaggle", "C:\\Users\\shlom\\OneDrive\\Documents\\Git Projects\\group-project-1\\normalizer");
        all[1]= new ImbdImporter("C:\\Users\\shlom\\OneDrive\\Documents\\Git Projects\\group-project-1\\imdb", "C:\\Users\\shlom\\OneDrive\\Documents\\Git Projects\\group-project-1\\normalizer");
        all[2]= new Normalizer("C:\\\\Users\\\\shlom\\\\OneDrive\\\\Documents\\\\Git Projects\\\\group-project-1\\\\normalizer","C:\\Users\\shlom\\OneDrive\\Documents\\Git Projects\\group-project-1\\validator");
        all[3]= new Validator("C:\\Users\\shlom\\OneDrive\\Documents\\Git Projects\\group-project-1\\validator","C:\\Users\\shlom\\OneDrive\\Documents\\Git Projects\\group-project-1\\deduper");
        all[4]= new Deduper("C:\\Users\\shlom\\OneDrive\\Documents\\Git Projects\\group-project-1\\deduper","C:\\Users\\shlom\\OneDrive\\Documents\\Git Projects\\group-project-1\\single_unified_file");

        ImportPipeline ip = new ImportPipeline();
        ip.processAll(all);
    }

    public void processAll(Processor[] all) throws IOException{
        for(int i = 0; i < all.length; i++) {
            all[i].execute();
        }
    }

}