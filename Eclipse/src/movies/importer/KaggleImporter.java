package movies.importer;
import java.util.ArrayList;
/**
 * KaggleImporter extends the Processor class.
 * It organizes the movies in the file in
 * which they are found and classifies them
 * under a common source.
 * 
 * 
 * @author Shlomo Bensimhon
 * @since	2020-11-08
 *
 *
 */

public class KaggleImporter extends Processor{

	public KaggleImporter(String sourceDir, String outputDir) {
		super(sourceDir, outputDir, true);
	}
	
	public ArrayList<String> process(ArrayList<String> input){
		ArrayList<String> tempList = new ArrayList<String>();

		String[] tabbedList;
		
		for(int i = 0; i < input.size(); i++) {
			tabbedList = input.get(i).toString().split("\\t", -1);
			Movie tempMovie = new Movie(tabbedList[12], tabbedList[15], tabbedList[13], "kaggle");
			tempList.add(tempMovie.toString());
		}
		return tempList;
	}
}
