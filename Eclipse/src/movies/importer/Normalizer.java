package movies.importer;

import java.util.ArrayList;
/**
 * Normalizer extends the Processor class.
 * It normalizes the movies in the file in
 * which they are found by removing capitals 
 * and getting rid of unnecessary data.
 * 
 * @author Shlomo Bensimhon
 * @since	2020-11-08
 *
 */

public class Normalizer extends Processor{
	/**
	 * 
	 * @param sourceDir
	 * @param outputDir
	 */
	public Normalizer(String sourceDir, String outputDir) {
		super(sourceDir, outputDir, false);
	}
/**
 * @param
 */
	public ArrayList<String> process(ArrayList<String> input){
		ArrayList<String> tempList = new ArrayList<String>();
		String[] tabbedList;
		for(int i = 0; i < input.size(); i++) {
			tabbedList = input.get(i).toString().split("\\t", -1);
			String[] tempRuntime = (tabbedList[2].toString().split(" "));
			Movie tempMovie = new Movie(tabbedList[1], tabbedList[0].toLowerCase(), tempRuntime[0], "kaggle");
			tempList.add(tempMovie.toString());
		}
		return tempList;
	}
}
		
		
