package movies.importer;
import java.util.ArrayList;
/**
 * ImbdImporter extends the Processor class.
 * It organizes the movies in the file in
 * which they are found and classifies them
 * under a common source.
 * 
 * @author Aharon Moryoussef 
 * @since	2020-11-08
 *
 */

public class ImbdImporter extends Processor{

	public ImbdImporter(String src , String destination) {
		super(src,destination,true);
		}

	public ArrayList<String> process (ArrayList<String> input) {
		String[] temp;
		ArrayList<String> transform = new ArrayList<String>();
		 for(int i = 0; i < input.size(); i++) {
			temp = input.get(i).toString().split("\\t",-1);
			Movie movie =  new Movie (temp[3],temp[1],temp[6],"imbd");
			transform.add(movie.toString());
		 }
	  
		
	return transform;
	}

}
