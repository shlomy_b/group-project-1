package movies.tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import movies.importer.Movie;
/**
 * MovieTest is a Junit test file for the movie class.
 * 
 * @author Aharon Moryoussef/Shlomo Bensimhon
 * @since	2020-11-08
 *
 */

class MovieTest {

	@Test
	public void movieSettersTest() {
		Movie m1 = new Movie("MadMax", "2015", "1:30", "Netflix");
		m1.setMovieName("MadMax");
		assertEquals("MadMax", m1.getMovieName());
		m1.setRelease("2015");
		assertEquals("2015", m1.getRelease());
		m1.setRuntime("1:30");
		assertEquals("1:30", m1.getRuntime());
		m1.setSource("Netflix");
		assertEquals("Netflix", m1.getSource());
		
	}
	
	@Test
	public void toStringTest() {
	Movie m1 = new Movie("MadMax", "2015", "1:30", "Netflix");
	String s1 = m1.toString();
	s1.equals(m1.toString());
	assertEquals(s1,m1.toString());
		
	}
}
