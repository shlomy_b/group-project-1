package movies.tests;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import movies.importer.*;
import java.io.IOException;
import java.util.ArrayList;
import movies.importer.Validator;
/**
 * This is a test class made for
 * the Validator class
 * 
 * 
 * @author Aharon Moryoussef
 * @since	2020-11-08
 *
 */

class ValidatorTest {

    @Test
    void testProcess() {

    }

    @Test
    void testValidator() {
    //    fail("Not yet implemented");
    }

    @Test
    void testProcessor() {
        ArrayList<String> test = new ArrayList<String>();
        ArrayList<String> results = new ArrayList<String>();
        test.add("MadMax" + "\t"+ "2015" + "\t" + "130" + "\t" + "imdb"); //good
        test.add("" + "\t"+ "2015" + "\t" + "130" + "\t" + "imdb");
        test.add("MadMax" + "\t"+ "la" + "\t" + "130" + "\t" + "imdb");
        test.add("MadMax" + "\t"+ "2015" + "\t" + "la" + "\t" + "imdb");
        test.add("Spiderman" + "\t"+ "2018" + "\t" + "130" + "\t" + "imdb"); //good
        test.add("" + "\t"+ "2018" + "\t" + "130" + "\t" + "imdb");
        test.add("Spiderman" + "\t"+ "hello" + "\t" + "130" + "\t" + "imdb");
        test.add("Spiderman" + "\t"+ "2018" + "\t" + "1:30" + "\t" + "imdb");
        results.add("MadMax" + "\t"+ "2015" + "\t" + "130" + "\t" + "imdb");
        results.add("Spiderman" + "\t"+ "2018" + "\t" + "130" + "\t" + "imdb");
        System.out.println(results.toString());

        assertEquals(true, results.toString().equals(test.toString()));

    }

}